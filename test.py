import asyncio
import json
import logging
import sys

from aiogram import Bot, Dispatcher, executor, types
from sqlalchemy import event, select
from sqlalchemy.orm import Session

from models.base import BaseModel
from models.database import Database
from models.game.game_state import GameState
from models.user import User
from views.overview import OverviewView


# Fix `bot.close()` on Windows (source: https://github.com/encode/httpx/issues/914#issuecomment-622586610)
if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())


with open("config.json", "rt", encoding="utf-8") as config_file:
    config = json.load(config_file)
    BOT_TOKEN = config["token"]

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=BOT_TOKEN)
dp = Dispatcher(bot)

db_engine = Database().engine

@dp.message_handler(commands='start')
async def start_cmd_handler(message: types.Message):
    user = message.from_user

    # TODO: refactor
    with Session(db_engine) as session:
        db_user = session.get(User, user.id)

        if not db_user:
            db_user = User(
                id=user.id,
                joined_on=message.date
            )

            session.add(db_user)
            session.commit()
        
        if db_user.game_id is None:
            db_game = GameState(
                gold=0,
                users=[db_user]
            )
            
            session.add(db_game)

            db_user.game_id = db_game.id
            session.commit()
        else:
            db_game = session.get(GameState, db_user.id)

    await OverviewView(db_engine, dp).display(message)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
