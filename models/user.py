from datetime import datetime
from sqlalchemy import Column, Integer, String, ForeignKey, BigInteger, DateTime
from sqlalchemy.orm import relationship
from .base import BaseModel

class User(BaseModel):
    __tablename__ = "users"

    id = Column(BigInteger, primary_key=True)  # matches Telegram id
    joined_on = Column(DateTime, nullable=False)

    game_id = Column(Integer, ForeignKey('game_states.id'))
    game = relationship("GameState", back_populates="users")

    def __repr__(self):
        return f"User(id={self.id})"
