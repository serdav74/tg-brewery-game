from sqlalchemy import create_engine, event
import os
from base import BaseModel


class Database:
    """This class initializes and provides a single database engine"""
    engine = None

    def __init__(self):
        if self.engine is None:
            self.production = bool(os.getenv('BREWERY_DB_NAME'))
            url = self.get_connection_string()

            self.engine = create_engine(url, future=True)
            if 'sqlite' in url:
                # foreign key support is disabled by default in sqlite
                event.listen(self.engine, 'connect', lambda c, _: c.execute('pragma foreign_keys=on'))
            
            BaseModel.metadata.create_all(self.engine)

    def get_connection_string(self):
        if self.production:
            raise NotImplementedError()
            return "postgresql+psycopg2://user:password@host:port/dbname[?key=value&key=value...]"
        else:
            return "sqlite:///sqlite.db"


