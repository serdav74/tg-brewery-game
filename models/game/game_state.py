from datetime import datetime
from sqlalchemy import Column, Integer, String, Numeric
from sqlalchemy.orm import relationship
from ..base import BaseModel

class GameState(BaseModel):
    __tablename__ = 'game_states'

    id = Column(Integer, primary_key=True)
    gold = Column(Numeric(18, 0), nullable=False)
    
    users = relationship("User", back_populates="game")

    def __repr__(self):
        users = (repr(user) for user in self.users)
        return f"GameState(id={self.id}, users={list(users)})"
