import logging
import typing

from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.callback_data import CallbackData
from models import GameState, User
from sqlalchemy.orm import Session

from .base import BaseView


class OverviewView(BaseView):
    def post_init(self):
        super().post_init()
        self.dispatcher.register_callback_query_handler(
            self.callback_handler,
            self.callback_data.filter(action=['inc', 'dec'])
        )

    callback_data = CallbackData('counter', 'action')

    def get_keyboard(self):
        return types.InlineKeyboardMarkup().row(
            types.InlineKeyboardButton('-1', callback_data=self.callback_data.new(action='dec')),
            types.InlineKeyboardButton('+1', callback_data=self.callback_data.new(action='inc')),
        )
    
    async def callback_handler(self, query: types.CallbackQuery, callback_data: typing.Dict[str, str]):
        logging.info('Got this callback data: %r', callback_data)  # callback_data contains all info from callback data
        await query.answer()  # don't forget to answer callback query as soon as possible
        action = callback_data['action']

        with Session(self.db_engine) as session:
            db_user = session.get(User, query.from_user.id)
            db_game = session.get(GameState, db_user.game_id)

                
            if action == 'inc':
                db_game.gold += 1
            else:
                db_game.gold -= 1
            
            session.commit()

        await self.display(query.message, query)

    async def display(self, message: types.Message, callback_query: typing.Optional[types.CallbackQuery] = None):
        if message.from_user.id != self.dispatcher.bot.id:
            user = message.from_user
        else:
            if callback_query:
                user = callback_query.from_user
            else:
                raise RuntimeError('callback_query is required when message.from_user == this bot')
        
        with Session(self.db_engine) as session:
            db_user = session.get(User, user.id)
            db_game = session.get(GameState, db_user.game_id)

            content = f"You have {db_game.gold} 🪙"
        
        if message.from_user.id == self.dispatcher.bot.id:
            await self.dispatcher.bot.edit_message_text(
                content,
                message.chat.id,
                message.message_id,
                reply_markup=self.get_keyboard()
            )
        else:
            await message.reply(
                content,
                reply_markup=self.get_keyboard()
            )

