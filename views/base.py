from aiogram import Dispatcher
from sqlalchemy.engine import Engine

class BaseView():
    def __init__(self, db_engine: Engine, bot_dispatcher: Dispatcher):
        self.db_engine = db_engine
        self.dispatcher = bot_dispatcher
        self.post_init()

    def post_init(self):
        pass